using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class escudomove : MonoBehaviour
{
   // public GameObject jugador;
    private GameObject escudo;
    private Rigidbody2D rg;
    private Boolean derecha = true;
    private Boolean agachado = false;
    // Start is called before the first frame update
    void Start()
    {
        escudo = this.GameObject();
        rg = this.GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D)){

            derecha = true;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            derecha=false;
        }
        if (Input.GetKey(KeyCode.S))
        {
            if (!agachado)
            {
                Agacha();
            }
            

        }
        else
        {
            if (agachado)
            {
                Agacha();
            }
            
        }


    }

    private void Agacha()
    {
        if (!agachado)
        {
            
            escudo.transform.position = new Vector3(escudo.transform.position.x, (escudo.transform.position.y -((float)0.9)),escudo.transform.position.z);
           agachado=true;
        }else if (agachado)
        {
            escudo.transform.position = new Vector3(escudo.transform.position.x, (escudo.transform.position.y + ((float)0.9)), escudo.transform.position.z);
            agachado = false;
        }
    }
}
