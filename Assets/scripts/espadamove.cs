using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class espadamove : MonoBehaviour
{
    private GameObject espada;
    public int poder;
    private Rigidbody2D rg;
    private Boolean derecha = true;
    private Boolean agachado = false;
    private Boolean atacando = false;
    // Start is called before the first frame update
    void Start()
    {
        espada = this.GameObject();
        rg = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {

            derecha = true;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            derecha = false;
        }
        
        if (Input.GetKey(KeyCode.S))
        {
            if (!agachado)
            {
                Agacha();
            }


        }
        else
        {
            if (agachado)
            {
                Agacha();
            }

        }
        if (Input.GetKey(KeyCode.Space))
        {

            if (!atacando)
            {
                Atacar();
            }
        }
        else
        {
            if (atacando)
            {
                Atacar();
            }
        }


    }

    private void Agacha()
    {
        if (!agachado)
        {

            espada.transform.position = new Vector3(espada.transform.position.x, (espada.transform.position.y - ((float)0.9)), espada.transform.position.z);
            agachado = true;
        }
        else if (agachado)
        {
            espada.transform.position = new Vector3(espada.transform.position.x, (espada.transform.position.y + ((float)0.9)), espada.transform.position.z);
            agachado = false;
        }
    }

    private void Atacar()
    {
        if (derecha) { 

        if (!atacando)
        {

            espada.transform.position = new Vector3((espada.transform.position.x + ((float)1.0)), espada.transform.position.y, espada.transform.position.z);
            atacando = true;
        }
        else if (atacando)
        {
            espada.transform.position = new Vector3((espada.transform.position.x - ((float)1.0)), espada.transform.position.y, espada.transform.position.z);
            atacando = false;
        }
        }else if(!derecha)
        {
            if (!atacando)
            {

                espada.transform.position = new Vector3((espada.transform.position.x - ((float)1.0)), espada.transform.position.y, espada.transform.position.z);
                atacando = true;
            }
            else if (atacando)
            {
                espada.transform.position = new Vector3((espada.transform.position.x + ((float)1.0)), espada.transform.position.y, espada.transform.position.z);
                atacando = false;
            }
        }
    }
}