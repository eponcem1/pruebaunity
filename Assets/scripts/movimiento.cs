using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class movimiento : MonoBehaviour
{
    // Start is called before the first frame update
    public int vel;
    private GameObject player;
    private Rigidbody2D rg;
    private Animator anim;
    private Boolean derecha = true;
    private Boolean jump = false;
    private Boolean agachado = false;
    void Start()
    {
        player = this.GameObject();  
        rg = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D)&&agachado==false)
        {
            
            rg.velocity = new UnityEngine.Vector2(vel, rg.velocity.y);
                //anim.SetBool("running", true);
            

            if (!derecha)
            {
                Rotate();
            }
                
            
        }else if (Input.GetKey(KeyCode.A) && agachado == false)
        {
            
                rg.velocity = new UnityEngine.Vector2(-vel, rg.velocity.y);
                //anim.SetBool("running", true);
           
            if (derecha)
            {
                Rotate();
            }

        }
        else
        {
            agachado = false;
            rg.velocity = new UnityEngine.Vector2 (0, rg.velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.W)&&jump==false)
        {
            rg.AddForce(new UnityEngine.Vector2(0, 300));
            //anim.SetBool("jumping", true);
            jump= true;

        }
        if (Input.GetKey(KeyCode.Space))
        {
            

            //anim.SetBool("atack", true);
            


        }
        if (Input.GetKey(KeyCode.S))
        {
            rg.velocity = new UnityEngine.Vector2(0, rg.velocity.y);
            agachado = true;
            if (Input.GetKey(KeyCode.D))
            {

                if (!derecha)
                {
                    Rotate();
                }
            }
            else if (Input.GetKey(KeyCode.A))
            {

                if (derecha)
                {
                    Rotate();
                }
            }

        }

    }

    private void Rotate()
    {
        if (derecha)
        {
            player.transform.Rotate(0, -(180), 0);
            derecha = false;
        }else if (!derecha)
        {
            player.transform.Rotate(0,-(180), 0);
            derecha =true;
        }
        
        
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "ground")
        {
            jump = false;
        }
    }
   

}
